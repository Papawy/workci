import unittest
import main

class TestStringMethods(unittest.TestCase):

    def setUp(self):
        self.client = main.app.test_client()

    def test_main(self):
        ret = self.client.get("/")
        self.assertEqual(b"Hello Agile!", ret.data)

    def test_health(self):
        ret = self.client.get("/healthz")
        self.assertEqual(200, ret.status_code)

    def test_woof(self):
        ret = self.client.get("/woof")
        self.assertEqual(500, ret.status_code)
