from flask import Flask, jsonify

app = Flask(__name__)

@app.route("/")
def hello_devops():
    return "Hello Agile!"

@app.route("/healthz")
def health():
   ret = jsonify(service="up")
   ret.status_code = 200
   return ret

@app.route("/woof")
def woof():
    ret = jsonify(woof="woof")
    ret.status_code = 500
    return ret
